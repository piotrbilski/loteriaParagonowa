#!/usr/bin/perl
package LotteryHttp;
use warnings;
use strict;
use IO::Handle;
use LWP::UserAgent;
use Data::Dumper;
use Storable;
my $urls = {
  login => 'https://loteriaparagonowa.gov.pl/auth/login',
  coupons => ['https://loteriaparagonowa.gov.pl/moje-konto', '(?<date>\d{4}-\d{2}-\d{2}).+?(?<price>\d+\.\d{2}).+?(?<nr>\d+).+?>\s*(?<code>[A-Z0-9]+)\s*<'],
  winners => ['https://loteriaparagonowa.gov.pl/wyniki', '<tr.+?(?<date>\d{2}\.\d{2}\.\d{4}).+?>(?<name>[ąęółźżćA-Za-z ]+).+?>\s*(?<code>[A-Z0-9]+)\s*<.+?>(?<reward>[ąęółźżćA-za-z0-9 ]+)<.+?</tr>'],
  #winners => ['https://loteriaparagonowa.gov.pl/wyniki', '<tr.+?(\d{2}\.\d{2}\.\d{4}).+?>([A-Z0-9]+)<.+?</tr>'],
};

sub new {
  my $class = shift;
  my %arguments = @_;
  my $self = {};
    
  $self->{login} = $arguments{login} || "";
  $self->{password} = $arguments{password} || "";
  
  bless($self, $class);
  
  $self->login();
  return $self;

}

sub login {
  my $self = shift;
  my $browser = LWP::UserAgent->new();
  $browser->agent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36");
  $browser->env_proxy;
  $browser->cookie_jar({});
  $self->{browser} = $browser;
  my $response = $browser->get($urls->{login});
  my $content = $response->content;
  $content =~ /name="_token"\s+value="(.*)"/;
  my $token = $1;
  
  $response = $browser->post($urls->{login}, [email => $self->{login}, password => $self->{password}, _token => $token]);
   
  $self->{logged} = 1 if $response->content =~ /"success":true/;
  
  return $self->{logged}; 
  
}


sub read_available_periods {

  my $self = shift;
  my $type = shift;
  my $url = $urls->{$type}->[0];
  my $regexp = 'id="recipes-.*?"\s+value="([a-z0-9\.]+?)"';
  my @periods;
  
  my $response = $self->{browser}->get($url);
  my $content = $response->content;
  
  push @periods, $1 while ($content =~ /$regexp/sg)  ;

  return @periods;
  

  
}

sub read_coupons {

  my $self = shift;
  my $type = shift;
  my $period = shift || ""; 
  my $result;
  my $response;
  my $page=1;
  my $url;
  my @all_coupons;

  do
  {
    
    $url = "$urls->{$type}->[0]?type=$period&page=$page";
    STDOUT->printflush(".");
    $response = $self->{browser}->get($url);
    my $regexp = $urls->{$type}->[1];
                             
    $result = $self->add_my_codes($response->content, $regexp, $type);
    $page++;
  }
  while($result);
  print("\n");
  
  }
  

sub add_my_codes{
  
  my $self = shift; 
  my $content = shift;
  my $regexp = shift;
  my $type = shift;
  my @codes;             
  my $return_code = 0;        
  
  while ($content =~ /$regexp/sg){
    my $result = [$1, $2, $3, $4];

    $self->{codes}->{$type}->{$+{code}} = $result; 
    $return_code = 1;  
    
  }
  
  return $return_code;

}

# -------------------------------------- MAIN

use Term::ReadKey;
use Term::Menu;
use Text::Table;
use Switch;
use Storable;

my $storage_file = 'lottery.dat';
my $stored_data = retrieve($storage_file) if (-e $storage_file);
my $answer = 'n';
my %credentials;
if($stored_data){
    print "Czy uzyc poprzednich danych do logowania? y/N :";
    $answer = <STDIN>;
    chomp($answer);
    print "\n";
    
}

if (lc($answer) eq 'y'){
    %credentials = %{$stored_data};
    print "Login: $credentials{login}\n\n";
}
else
{
    print "Podaj login: ";
    my $login = <STDIN>;
    chomp($login);
    ReadMode('noecho');

    print "Podaj haslo: ";
    my $password = ReadLine('0');
    print "\n";
    ReadMode('normal');
    chomp($password);
    
    $credentials{login} = $login;
    $credentials{password} = $password;
}

store \%credentials, $storage_file;

my $object = LotteryHttp->new(%credentials);
print "Niewlasciwy login badz haslo" && exit(1) unless $object->{logged} ;

my $prompt = new Term::Menu;

while(1){

    my $options = $prompt->menu(
      read_coupons => ['Pobierz dodane kupony' , 'r'],
      read_winnings => ['Pobierz zwycieskie kupony', 'w'],
      display_coupons => ['Wyswietl dodane kupony','dy'],
      display_winnings => ['Wyswietl zwycieskie kupony', 'dw'],
      compare => ['Sprawdz czy wygrales', 'd'],
      finish => ['Zakoncz', 'e']
    );

    my $answer = $prompt->lastval;

    switch ($answer) {
    
      case 'read_coupons'{ 
        my @periods = $object->read_available_periods('coupons');
        for (@periods){
          $object->read_coupons('coupons',$_);
        }
       }
      case 'read_winnings'{ 
      $object->read_coupons('winners') 
      }
      case 'compare'{ 
        my @coupons = keys %{$object->{codes}->{coupons}};
        my $tab = Text::Table->new("DATA LOSOWANIA", "DANE LAUREATA", "NR ZGLOSZENIA", "NAGRODA");
        for my $coupon (@coupons){
          if (exists $object->{codes}->{winners}->{$coupon}){
            my $record = $object->{codes}->{winners}->{$coupon};
            $tab->add($record->[0], $record->[1], $record->[2], $record->[3]);    
          } 
          
        }
        print "\n Twoje zwycieskie kupony to: \n\n";
        print $tab;
        print "\n";

      }
      case 'display_coupons' {
         my $tab = Text::Table->new("DATA","KWOTA BRUTTO","NR WYDRUKU","NR ZGLOSZENIA");
         my @codes = keys %{$object->{codes}->{coupons}};
         for my $entry (@codes){
          my $record = $object->{codes}->{coupons}->{$entry};
          $tab->add($record->[0], $record->[1], $record->[2], $record->[3]);
         
         }
         print $tab;
         print "\n";
         my $count = @codes;
         print "Zarejestrowanych kuponow: $count\n\n";
      }
      case 'display_winnings' {
         
         my $tab = Text::Table->new("DATA LOSOWANIA", "DANE LAUREATA", "NR ZGLOSZENIA", "NAGRODA");
         my @codes = keys %{$object->{codes}->{winners}};
         
         for my $entry (@codes){
          my $record = $object->{codes}->{winners}->{$entry};
          $tab->add($record->[0], $record->[1], $record->[2], $record->[3]);
         
         }
         print $tab;
         print "\n";
         my $count = @codes;
         print "Zarejestrowanych wygranych kuponow: $count\n\n";
         
      }
      case 'finish' {
        exit(0);
      }

    }
}


1;
